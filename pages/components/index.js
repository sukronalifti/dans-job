import React from 'react'
import MainLayout from '../../src/components/layout/MainLayout'
import Components from '../../src/containers/admin/components'

export default function index() {
  return (
    <>
      <MainLayout>
        <Components />
      </MainLayout>
    </>
  )
}
