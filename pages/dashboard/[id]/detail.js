import React from "react";
import MainLayout from "../../../src/components/Layout/MainLayout";
import DetailJob from "../../../src/containers/admin/dashboard/component/DetailJob";

export default function Index() {
    return (
        <>
         <MainLayout>
            <DetailJob />
         </MainLayout>
        </>
    )
}