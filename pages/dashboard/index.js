import React from 'react'
import MainLayout from '../../src/components/Layout/MainLayout'
import Dashboard from '../../src/containers/admin/dashboard'

export default function index() {
  return (
    <>
      <MainLayout>
        <Dashboard />
      </MainLayout>
    </>
  )
}
