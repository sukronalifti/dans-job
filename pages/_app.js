import { ChakraProvider } from '@chakra-ui/react'
import '../src/assets/styles/main.scss'
import theme from '../chakraconfig'
import { useEffect } from 'react'

import store from '../src/store/store'
import { Provider } from 'react-redux'
import Head from 'next/head'

export default function App({ Component, pageProps }) {
  useEffect(() => {
    if (typeof window !== 'undefined') {
      const loader = document.getElementById('globalLoader')
      if (loader) {
        loader.remove()
      }
    }
  }, [])
  return (
    <>
      <Head>
        <title>GitHub Job</title>
      </Head>
      <Provider store={store()}>
        <ChakraProvider theme={theme}>
          <Component {...pageProps} />
        </ChakraProvider>
      </Provider>
    </>
  )
}
