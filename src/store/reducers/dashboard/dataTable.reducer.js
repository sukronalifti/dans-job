import { Types } from '../../../constants/Types'

const initialState = {
  totalCount: 0,
  items: [],
  totalPages: 0,
  data: null,
  loading: true,
}

const dataTable = (state = initialState, action) => {
  const payload = action.payload
  switch (action.type) {
    case Types.ISLOADING:
      return {
        ...state,
        loading: payload,
      }
    case Types.TABLE_MERGE:
      return {
        ...state,
        items: payload,
      }
    case Types.TABLE_TOTALPAGES:
      return {
        ...state,
        totalPages: payload,
      }
    case Types.DETAIL:
      return {
        ...state,
        data: payload,
      }
    default:
      return state
  }
}

export default dataTable
