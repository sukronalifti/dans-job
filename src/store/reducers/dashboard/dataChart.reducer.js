import { Types } from '../../../constants/Types'

const initialState = {
  items: [],
  loading: true,
}

const dataChart = (state = initialState, action) => {
  const payload = action.payload
  switch (action.type) {
    case Types.ISLOADING:
      return {
        ...state,
        loading: payload,
      }
    case Types.CHART_MERGE:
      return {
        ...state,
        items: payload,
      }
    default:
      return state
  }
}

export default dataChart
