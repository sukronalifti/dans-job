import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import dataTable from './dashboard/dataTable.reducer'
import dataChart from './dashboard/dataChart.reducer'

const persistConfig = {
  key: 'Root',
  storage,
}

const rootReducer = combineReducers({
  dataTable,
  dataChart,
})

export default persistReducer(persistConfig, rootReducer)
