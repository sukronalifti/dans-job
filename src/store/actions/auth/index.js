import { APIAUTH } from '../../../helpers/API'
import Cookies from 'js-cookie'

export const loginUserExt = (payload) => (dispatch) =>
  new Promise((resolve, reject) => {
    dispatch({ type: 'ISLOADING', payload: true })
    APIAUTH()
      .loginUser(payload)
      .then((response) => {
        const result = response.data
        const inFifteenMinutes = new Date(new Date().getTime() + 58 * 60 * 1000)
        Cookies.set('userData', JSON.stringify(result.data), { expires: inFifteenMinutes })
        Cookies.set('accessToken', result.data.access_token, { expires: inFifteenMinutes })
        resolve(response)
      })
      .catch(function (error) {
        reject(error)
      })
      .finally(() => {
        dispatch({ type: 'ISLOADING', payload: false })
      })
  })

export const loginUserSSO = (payload) => (dispatch) =>
  new Promise((resolve, reject) => {
    dispatch({ type: 'ISLOADING', payload: true })
    APIAUTH()
      .loginUserSSO(payload)
      .then((response) => {
        const inFifteenMinutes = new Date(new Date().getTime() + 58 * 60 * 1000)
        Cookies.set('userData', JSON.stringify(result), { expires: inFifteenMinutes })
        Cookies.set('accessToken', result.access_token, { expires: inFifteenMinutes })
        resolve(response)
      })
      .catch(function (error) {
        reject(error)
      })
      .finally(() => {
        dispatch({ type: 'ISLOADING', payload: false })
      })
  })

