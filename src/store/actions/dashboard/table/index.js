import { APIAUTH } from '../../../../helpers/API'

export const getTableData = (payload) => (dispatch) =>
  new Promise((resolve, reject) => {
    dispatch({ type: 'ISLOADING', payload: true })
    APIAUTH()
      .getApiListJob(payload)
      .then((response) => {
        resolve(response)
        dispatch({ type: 'TABLE_MERGE', payload: response })
      })
      .catch(function (error) {
        reject(error)
      })
      .finally(() => {
        dispatch({ type: 'ISLOADING', payload: false })
      })
  })
  
export const filterTableData = (payload) => (dispatch) =>
  new Promise((resolve, reject) => {
    dispatch({ type: 'ISLOADING', payload: true })
    APIAUTH()
      .filterApiListJob(payload)
      .then((response) => {
        resolve(response)
        dispatch({ type: 'TABLE_MERGE', payload: response })
      })
      .catch(function (error) {
        reject(error)
      })
      .finally(() => {
        dispatch({ type: 'ISLOADING', payload: false })
      })
  })

export const detailTableData = (payload) => (dispatch) =>
  new Promise((resolve, reject) => {
    dispatch({ type: 'ISLOADING', payload: true })
    APIAUTH()
      .detailListJob(payload)
      .then((response) => {
        resolve(response)
        dispatch({ type: 'DETAIL', payload: response?.data })
      })
      .catch(function (error) {
        reject(error)
      })
      .finally(() => {
        dispatch({ type: 'ISLOADING', payload: false })
      })
  })

