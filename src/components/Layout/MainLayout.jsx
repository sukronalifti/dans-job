import { useState } from 'react'
import { Box, useColorModeValue, useDisclosure } from '@chakra-ui/react'
import { Header } from './Navbar/Header'

export default function MainLayout({ children }) {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const [expand, setIsExpand] = useState(true)

  const handleExpand = () => {
    setIsExpand(!expand)
  }

  return (
    <Box minH="100vh" bg={useColorModeValue('gray.100', 'gray.900')}>
      <Header onOpen={handleExpand} isExpand={expand} />
      <Box
        transition="width 5s ease"
        ml={expand ? { base: 0, md: 10 } : { base: 0, md: 20 }}
        p="4"
      >
        <Box marginTop="3.5rem">{children}</Box>
      </Box>
    </Box>
  )
}
