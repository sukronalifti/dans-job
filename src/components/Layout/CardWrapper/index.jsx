import { Box } from '@chakra-ui/react'

export default function CardWrapper({ children }) {
  return (
    <Box bg="white" padding="20px" borderRadius="md" shadow="lg">
      {children}
    </Box>
  )
}
