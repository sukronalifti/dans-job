import Link from 'next/link'
import styles from './sidebar.module.scss'
import { Box, Flex } from '@chakra-ui/react'
import { useEffect, useState } from 'react'
import { FaChevronDown, FaChevronRight } from 'react-icons/fa'
import { Menus } from '../../../../constants/Menus.js'
import { useRouter } from 'next/router'

export const SidebarContent = ({ isExpand }) => {
  const [showDropdown, setShowDropdown] = useState(false)
  const [menus, setMenus] = useState([])
  const { wrapperItems, sidebar, active } = styles

  const { asPath } = useRouter()

  useEffect(() => {
    setMenus(Menus)
  }, [])

  const handleDropdown = () => {
    setShowDropdown(!showDropdown)
  }

  const MenuItem = ({ link }) => (
    <Flex justifyContent="space-between" alignItems="center">
      <Flex alignItems="center" gap="13px">
        <Box>{link.icon}</Box>
        <Box
          display={
            !isExpand
              ? { base: 'block', md: 'none' }
              : { base: 'block', md: 'block' }
          }
        >
          {link.label}
        </Box>
      </Flex>
      <Box>
        {isExpand &&
          link.subMenu &&
          (showDropdown ? <FaChevronDown /> : <FaChevronRight />)}
      </Box>
    </Flex>
  )

  return (
    <Flex
      className={sidebar}
      borderColor="gray.200"
      w={isExpand ? '240px' : '75px'}
      display={isExpand ? { base: 'none', md: 'block' } : { md: 'block' }}
    >
      <Flex
        flexDir="column"
        w="100%"
        alignItems={isExpand ? 'flex-start' : 'center'}
        as="nav"
      >
        <Box className={wrapperItems}>
          <Flex mx="15px" my="10px" flexDir="column" gap="1">
            {menus.map((link, idx) => (
              <Box key={idx}>
                <Box
                  className={asPath === link.href ? active : ''}
                  align="center"
                  p="3"
                  borderRadius="lg"
                  role="group"
                  cursor="pointer"
                  transition="ease"
                  transitionDuration="0.3s"
                  fontSize="sm"
                  _hover={{
                    pl: '20px',
                    bg: 'green.500',
                    color: 'white',
                  }}
                >
                  <Box onClick={link.subMenu && handleDropdown}>
                    {link.href !== '#' ? (
                      <Link href={link.href}>
                        <MenuItem link={link} />
                      </Link>
                    ) : (
                      <MenuItem link={link} />
                    )}
                  </Box>
                </Box>
                <Box>
                  {isExpand &&
                    showDropdown &&
                    link.subMenu?.map((link, idx) => {
                      return (
                        <Box
                          key={idx}
                          className={asPath === link.href ? active : ''}
                          align="center"
                          p="3"
                          borderRadius="lg"
                          role="group"
                          cursor="pointer"
                          transition="ease"
                          transitionDuration="0.3s"
                          fontSize="sm"
                          _hover={{
                            pl: '20px',
                            bg: 'green.500',
                            color: 'white',
                          }}
                        >
                          <Link href={link.href}>
                            <Flex marginLeft="7">{link.label}</Flex>
                          </Link>
                        </Box>
                      )
                    })}
                </Box>
              </Box>
            ))}
          </Flex>
        </Box>
      </Flex>
    </Flex>
  )
}
