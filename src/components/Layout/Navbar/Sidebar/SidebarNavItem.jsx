import {
  Flex,
  Menu,
  MenuButton,
  Icon,
  MenuList,
  Text,
  UnorderedList,
  ListItem,
  Box,
} from '@chakra-ui/react'
import Link from 'next/link'

export default function SidebarNavItem({
  icon,
  title,
  active,
  href,
  navSize,
  children,
  ...props
}) {
  return (
    <UnorderedList>
      <ListItem _hover={{ width: '4px', opacity: 1, visibility: 'visible' }}>
        <Link href={href} {...props}>
          <Flex
            flexDir="column"
            w="100%"
            alignItems={navSize == 'small' ? 'center' : 'flex-start'}
          >
            <Menu placement="right">
              <Flex
                backgroundColor={active && '#AEC8CA'}
                w={navSize == 'large' && '100%'}
              >
                <MenuButton w="100%">
                  <Flex alignItems="center" justifyContent="space-between">
                    <Flex alignContent="center">
                      {icon}
                      <Text
                        display={navSize == 'small' ? 'none' : 'flex'}
                        fontSize="sm"
                        ml={5}
                      >
                        {title}
                      </Text>
                    </Flex>
                    <Flex alignItems="center">{children}</Flex>
                  </Flex>
                </MenuButton>
              </Flex>
            </Menu>
          </Flex>
        </Link>
      </ListItem>
    </UnorderedList>
  )
}
