import {
  Avatar,
  Box,
  Flex,
  HStack,
  IconButton,
  Image,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
  Show,
  useColorModeValue,
  VStack,
} from '@chakra-ui/react'
import Link from 'next/link'
import { FiBell, FiChevronDown, FiChevronRight, FiMenu } from 'react-icons/fi'
import styles from './header.module.scss'
import { getUserData } from '../../../../helpers/utils' 

export const Header = ({ onOpen, isExpand, ...rest }) => {
  const { header, iconButton } = styles
  const userData = getUserData()

  return (
    <Flex
      className={header}
      px={{ base: 4, md: 4 }}
      style={{ height: '55px' }}
      bg={useColorModeValue('blue.600')}
      borderBottomColor={useColorModeValue('gray.200', 'gray.700')}
      {...rest}
    >
      <Flex alignItems="center">
        <Link href="/">
          <Text as='b' fontSize='30px' color='white'>GitHub Jobs</Text>
        </Link>
      </Flex>

      <HStack spacing={{ base: '0', md: '6' }}>
        <IconButton
          size="lg"
          variant="ghost"
          aria-label="open menu"
          icon={<FiBell />}
        />
        <Flex alignItems={'center'} height={5}>
          <Menu>
            <MenuButton
              py={2}
              transition="all 0.3s"
              _focus={{ boxShadow: 'none' }}
            >
              <HStack>
                <Avatar size={'sm'} src={''} />
                <VStack
                  display={{ base: 'none', md: 'flex' }}
                  alignItems="flex-start"
                  spacing="1px"
                  ml="2"
                >
                  <Text fontSize="xs" fontWeight="bold" color="gray.600">
                    {userData?.nama}
                  </Text>
                </VStack>
                <Box display={{ base: 'none', md: 'flex' }}>
                  <FiChevronDown />
                </Box>
              </HStack>
            </MenuButton>
            <MenuList
              bg={useColorModeValue('white', 'gray.900')}
              borderColor={useColorModeValue('gray.200', 'gray.700')}
            >
              <MenuItem
                fontSize="sm"
                _hover={{ bg: 'green.400', color: 'white' }}
              >
                Sign out
              </MenuItem>
            </MenuList>
          </Menu>
        </Flex>
      </HStack>
    </Flex>
  )
}
