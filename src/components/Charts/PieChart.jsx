import { Card, Text } from '@chakra-ui/react'
import { useEffect } from 'react'
import {
  ResponsiveContainer,
  PieChart,
  Pie,
  Cell,
  Tooltip,
  Legend,
} from 'recharts'

export default function ChartPie({ data }) {
  useEffect(() => {
    data.forEach((item) => (item.priceUsd = parseInt(item.priceUsd)))
  }, [data])

  const COLORS = [
    '#28ABB9',
    '#2D6187',
    '#A8DDA8',
    '#4D96FF',
    '#557B83',
    '#39AEA9',
    '#7FBCD2',
  ]

  const RADIAN = Math.PI / 180
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index,
    value,
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5
    const x = cx + radius * Math.cos(-midAngle * RADIAN)
    const y = cy + radius * Math.sin(-midAngle * RADIAN)

    return (
      <>
        <text
          x={x}
          y={y}
          fill="white"
          textAnchor={x > cx ? 'start' : 'end'}
          dominantBaseline="central"
        >
          {`${(percent * 100).toFixed(0)} %`} - {value}
        </text>
      </>
    )
  }
  return (
    <Card p={2} variant="outline" w="100%">
      <Text fontWeight="bold" mb={5}>
        Pie Chart
      </Text>
      <ResponsiveContainer width="100%" height={400}>
        <PieChart width={300} height={300}>
          <Pie
            data={data}
            labelLine={false}
            label={renderCustomizedLabel}
            outerRadius={100}
            fill="#8884d8"
            dataKey="priceUsd"
          >
            {data?.map((entry, index) => (
              <Cell
                key={`cell-${index}`}
                fill={COLORS[index % COLORS.length]}
              />
            ))}
          </Pie>
          <Legend verticalAlign="bottom" />
        </PieChart>
      </ResponsiveContainer>
    </Card>
  )
}
