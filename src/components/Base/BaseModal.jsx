import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  Button,
  Lorem,
  ModalCloseButton,
  useDisclosure,
  Text,
  Box,
} from '@chakra-ui/react'
import BaseButton from './BaseButton'

export default function BaseModal({ title, body }) {
  const { isOpen, onClose, onOpen } = useDisclosure()
  return (
    <>
      <BaseButton onClick={onOpen}>Open Modal</BaseButton>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent mx={3}>
          <ModalHeader>{title}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text>{body}</Text>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="red" mr={3} onClick={onClose}>
              Close
            </Button>
            <Button variant="ghost">Secondary Action</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}
