import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Stack,
  Text,
} from '@chakra-ui/react'
import Link from 'next/link'
import React from 'react'
import { BsChevronRight } from 'react-icons/bs'
import { FaHome } from 'react-icons/fa'

export default function Breadcrumbs({ items, title, path, href }) {
  return (
    <>
      <Stack>
        <Text fontSize="xl" fontWeight="bold">
          {title}
        </Text>
        <Breadcrumb
          separator={<BsChevronRight color="gray.500" />}
          style={{ fontSize: '12px' }}
        >
          <BreadcrumbItem>
            <BreadcrumbLink href="#">
              <FaHome size={15} />
            </BreadcrumbLink>
          </BreadcrumbItem>
          {items.map((item, idx) => (
            <BreadcrumbItem key={idx}>
              <BreadcrumbLink href={href}>{item.title}</BreadcrumbLink>
            </BreadcrumbItem>
          ))}
        </Breadcrumb>
      </Stack>
    </>
  )
}
