import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    Center,
    useDisclosure,
    Progress,
    Text,
    Container
  } from '@chakra-ui/react'
  import BaseButton from './BaseButton'
  
  export default function BaseLoading({ title, body, loading }) {
    const { isOpen, onClose, onOpen } = useDisclosure()
    return (
      <>
        <Modal isOpen={loading} closeOnOverlayClick={false} onClose={onClose}>
          <ModalOverlay 
          backdropFilter='blur(5px) hue-rotate(90deg)'
          />
          <ModalContent>
            <ModalHeader>{title}</ModalHeader>
            <ModalBody>
                <Progress colorScheme='green' size='sm' isIndeterminate />
                <Container>
                 <Text fontSize='sm'>
                    <Center color='black'>
                        Silahkan Tunggu
                    </Center>
                 </Text>
                </Container>
            </ModalBody>
          </ModalContent>
        </Modal>
      </>
    )
  }
  