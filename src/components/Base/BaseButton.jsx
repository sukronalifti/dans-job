import { Button, Text } from '@chakra-ui/react'

export default function BaseButton({
  colorScheme = 'blue',
  title,
  children,
  color,
  ...props
}) {
  return (
    <Button colorScheme={colorScheme} size="sm" {...props}>
      <Text color={color}>{title}</Text>
      {children}
    </Button>
  )
}
