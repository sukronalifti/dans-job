import { useToast, Wrap, WrapItem } from '@chakra-ui/react'
import BaseButton from './BaseButton'

export default function BaseToast({ status, color, title }) {
  const toast = useToast()

  return (
    <Wrap>
      <WrapItem>
        <BaseButton
          title="Show Toast"
          colorScheme={color}
          onClick={() =>
            toast({
              title: `${title}`,
              position: 'top-right',
              status: status,
              duration: 2500,
              isClosable: true,
            })
          }
        />
      </WrapItem>
    </Wrap>
  )
}
