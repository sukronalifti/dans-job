import {
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react'
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useTable, usePagination } from 'react-table'
import { Pagination } from '../Pagination'
import styles from './table.module.scss'

export const DefaultTable = ({
  columns,
  data,
  pageIndex: pageIndexProp = 0,
  pageSize: pageSizeProp = 10,
  totalCount,
  pageCount: pageCountProp,
  setPage = () => {},
}) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    pageOptions,
    state,
    gotoPage,
    pageCount,
    prepareRow,
    setPageSize,
    nextPage,
    previousPage,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: {
        pageIndex: pageIndexProp,
        pageSize: pageSizeProp,
      },
      manualPagination: true,
      pageCount: pageCountProp,
    },
    usePagination,
  )

  useEffect(() => {
    setPage(pageIndex)
  }, [pageIndex])

  const { trField } = styles

  const filterPages = (visiblePages, totalPages) => {
    return visiblePages.filter((page) => page <= totalPages)
  }

  const getVisiblePage = (page, total) => {
    if (total < 7) {
      return filterPages([1, 2, 3, 4], total)
    } else {
      if (page % 5 >= 0 && page > 4 && page + 2 < total) {
        return [1, page - 1, page, page + 1, total]
      } else if (page % 5 >= 0 && page > 4 && page + 2 >= total) {
        return [1, total - 3, total - 2, total - 1, total]
      } else {
        return [1, 2, 3, total]
      }
    }
  }

  const [visiblePage, setVisiblePage] = useState([])

  useEffect(() => {
    const visible = getVisiblePage(pageIndex + 1, pageCount)
    if (visiblePage.length < 1) {
      setVisiblePage(filterPages(visible, pageCount))
    }
  }, [])

  const changePage = (i) => {
    const activePage = pageCount + 1

    if (i === activePage) return

    const visiblePages = getVisiblePage(i, pageCount)
    setVisiblePage(filterPages(visiblePages, pageCount))

    gotoPage(i - 1)
  }

  return (
    <>
      <TableContainer borderRadius="md" whiteSpace="inherit">
        <Table {...getTableProps()} variant="simple" bg="white">
          <Thead bg="green.600">
            {headerGroups.map((headerGroup) => (
              <Tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <Th
                    {...column.getHeaderProps({ style: { width: '200px' } })}
                    fontSize="xs"
                    color="white"
                  >
                    {column.render('Header')}
                  </Th>
                ))}
              </Tr>
            ))}
          </Thead>
          <Tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row)
              return (
                <Tr {...row.getRowProps()} fontSize="xs">
                  {row.cells.map((cell) => {
                    return (
                      <Td {...cell.getCellProps()} className={trField}>
                        {cell.render('Cell')}
                      </Td>
                    )
                  })}
                </Tr>
              )
            })}
          </Tbody>
        </Table>
      </TableContainer>
      <Pagination
        pageCount={pageCount}
        pageIndex={pageIndex}
        changePage={changePage}
        pageOptions={pageOptions}
        visiblePage={visiblePage}
        nextPage={nextPage}
        previousPage={previousPage}
      />
    </>
  )
}
