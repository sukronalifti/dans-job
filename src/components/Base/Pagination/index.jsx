import { Box, Button, Flex, IconButton, Text } from '@chakra-ui/react'
import {
  FiChevronLeft,
  FiChevronRight,
  FiChevronsLeft,
  FiChevronsRight,
} from 'react-icons/fi'
import styles from './pagination.module.scss'

export const Pagination = ({
  pageIndex,
  pageOptions,
  pageCount,
  changePage,
  nextPage,
  previousPage,
}) => {
  const { iconButton, listNum } = styles

  const getVisiblePage = [
    ...(pageIndex > 1 ? [null] : []),
    ...[...Array(3).keys()]
      .map((idx) => idx + pageIndex)
      .filter((idx) => idx > 0 && idx <= pageCount),
    ...(pageIndex < pageCount - 2 ? [null] : []),
  ]
  return (
    <>
      <Box>
        <Flex
          justifyContent="space-between"
          alignItems="center"
          flexDir={{ base: 'column', md: 'row' }}
        >
          <Flex
            alignItems="center"
            gap="2px"
            flexDir={{ base: 'column', md: 'row' }}
            mb={{ base: 3 }}
          >
            <Text fontSize="xs" color="gray.500">
              Halaman {pageIndex + 1} dari
            </Text>
            <Text
              bgColor="gray.100"
              p={1}
              borderRadius="sm"
              color="green.600"
              fontSize="xs"
              fontWeight="bold"
            >
              {pageOptions.length}
            </Text>
          </Flex>
          <Box listStyleType="none" display="flex" className={listNum}>
            <Box>
              <IconButton
                className={iconButton}
                isDisabled={pageIndex === 0}
                _hover={{ backgroundColor: 'green.600', color: 'white' }}
                onClick={() => {
                  if (pageIndex === 1) return
                  changePage(1)
                }}
                icon={<FiChevronsLeft />}
              />
            </Box>
            <Box>
              <IconButton
                className={iconButton}
                isDisabled={pageIndex === 0}
                _hover={{ backgroundColor: 'green.600', color: 'white' }}
                onClick={() => {
                  previousPage()
                }}
                icon={<FiChevronLeft />}
              />
            </Box>

            {getVisiblePage.map((page, idx) => {
              if (page === null) {
                return (
                  <Box key={idx}>
                    <Button
                      isDisabled={true}
                      h="1.8rem"
                      borderWidth="1px"
                      borderRadius={0}
                    >
                      ...
                    </Button>
                  </Box>
                )
              } else {
                return (
                  <Box key={idx}>
                    <Button
                      fontSize="xs"
                      borderRadius={0}
                      h="1.8rem"
                      borderWidth="1px"
                      _hover={{ backgroundColor: 'green.600', color: 'white' }}
                      bgColor={page === pageIndex + 1 && 'green.500'}
                      color={page === pageIndex + 1 && 'white'}
                      onClick={changePage.bind(null, page)}
                    >
                      {page}
                    </Button>
                  </Box>
                )
              }
            })}
            <Box>
              <IconButton
                className={iconButton}
                isDisabled={pageIndex + 1 === pageOptions.length}
                _hover={{ backgroundColor: 'green.600', color: 'white' }}
                onClick={() => {
                  nextPage()
                }}
                icon={<FiChevronRight />}
              />
            </Box>
            <Box>
              <IconButton
                className={iconButton}
                isDisabled={pageIndex + 1 === pageOptions.length}
                _hover={{ backgroundColor: 'green.600', color: 'white' }}
                onClick={() => {
                  if (pageIndex + 1 === pageOptions.length) return
                  changePage(pageOptions.length)
                }}
                icon={<FiChevronsRight />}
              />
            </Box>
          </Box>
        </Flex>
      </Box>
    </>
  )
}
