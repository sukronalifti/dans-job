import { IconButton, Text } from '@chakra-ui/react'

export default function BaseIconButton({
  colorScheme,
  title,
  children,
  icon,
  color,
  ...props
}) {
  return (
    <IconButton colorScheme={colorScheme} size="sm" icon={icon} {...props} />
  )
}
