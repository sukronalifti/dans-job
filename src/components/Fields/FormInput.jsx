import React from 'react'
import { Stack, Input, Text, Skeleton } from "@chakra-ui/react"

function FormInput(props) {
  const {
    size,
    type,
    isRequired,
    id,
    name,
    onChange,
    placeholder,
    variant,
    error,
    isLoading
  } = props;
  return (
    <div>
       {isLoading ? (
        <Stack>
          <Skeleton height='20px' />
        </Stack>
     ) : (
        <Stack>
            <Input
             size={size}
             type={type}
             name={name}
             isRequired={isRequired}
             variant={variant}
             id={id}
             placeholder={placeholder}
             onChange={onChange}
             focusBorderColor='green.500'
            />
        {error && type !== 'date' && (
            <Stack>
              <Text color='red' fontSize='sm'>{error}</Text>
            </Stack>
          )}
        </Stack>
     )}
    </div>
  )
}

export default FormInput
