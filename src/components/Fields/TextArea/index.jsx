import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Textarea,
} from '@chakra-ui/react'
import { Field, useField } from 'formik'

export default function TextArea({ label, ...props }) {
  const [field, meta] = useField(props)
  return (
    <FormControl>
      <FormLabel fontSize="sm">{label}</FormLabel>
      <Field
        as={Textarea}
        size="sm"
        focusBorderColor="green.600"
        style={{ backgroundColor: 'white' }}
        {...field}
        {...props}
      />
      <FormErrorMessage>{meta.error}</FormErrorMessage>
    </FormControl>
  )
}
