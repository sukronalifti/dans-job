import {
  FormControl,
  FormErrorMessage,
  FormLabel,
} from '@chakra-ui/form-control'
import { Input } from '@chakra-ui/input'
import { Field, useField } from 'formik'

export default function TextField({ label, fontWeight, ...props }) {
  const [field, meta] = useField(props)
  return (
    <FormControl isInvalid={meta.error && meta.touched}>
      <FormLabel fontSize="sm" fontWeight={fontWeight}>
        {label}
      </FormLabel>
      <Field
        as={Input}
        size="sm"
        focusBorderColor="green.600"
        backgroundColor="white"
        {...field}
        {...props}
      />
      <FormErrorMessage>{meta.error}</FormErrorMessage>
    </FormControl>
  )
}
