import { Checkbox, Stack, Text } from '@chakra-ui/react'

export default function CheckboxField({ text }) {
  return (
    <Stack spacing={5} direction="row">
      <Checkbox colorScheme="green" fontSize="sm" defaultChecked>
        <Text fontSize="sm">{text}</Text>
      </Checkbox>
    </Stack>
  )
}
