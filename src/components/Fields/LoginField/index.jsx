import {
  FormControl,
  FormErrorMessage,
  FormLabel,
} from '@chakra-ui/form-control'
import { Input } from '@chakra-ui/input'
import { Field, useField } from 'formik'

export default function LoginField({ label, fontWeight, ...props }) {
  const [field, meta] = useField(props)
  return (
    <FormControl isInvalid={meta.error && meta.touched}>
      <FormLabel fontSize="sm" fontWeight="bold">
        {label}
      </FormLabel>
      <Field
        as={Input}
        size="sm"
        focusBorderColor="green.600"
        backgroundColor="gray.100"
        p="5"
        borderRadius="md"
        border="0"
        {...field}
        {...props}
      />
      <FormErrorMessage>{meta.error}</FormErrorMessage>
    </FormControl>
  )
}
