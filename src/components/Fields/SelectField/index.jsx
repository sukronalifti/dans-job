import { useId, useState } from 'react'
import { useEffect } from 'react'
import Select from 'react-select'

export default function SelectField({ options }) {
  const [selectedOption, setSelectedOption] = useState(null)

  useEffect(() => {
    setSelectedOption(options)
  }, [])

  return (
    <Select
      instanceId={useId()}
      styles={{
        control: (baseStyles, state) => ({
          ...baseStyles,
          borderColor:
            !state.isFocused && 'var(--chakra-colors-chakra-border-color)',
          fontSize: '12px',
          minHeight: 0,
          ':focus': {
            backgroundColor: '#B3FFAE',
          },
        }),
        options: (baseStyles, { isSelected }) => ({
          ...baseStyles,
          fontSize: '12px',
          ':hover': {
            backgroundColor: !isSelected && '#B3FFAE',
          },
        }),
        dropdownIndicator: (baseStyles) => ({
          ...baseStyles,
          height: '30px',
        }),
        option: (styles, { isFocused, isSelected }) => ({
          ...styles,
          color: isFocused ? 'black' : isSelected ? 'white' : undefined,
          background: isFocused
            ? 'var(--chakra-colors-green-100)'
            : isSelected
            ? 'var(--chakra-colors-green-400)'
            : undefined,
          fontSize: '12px',
        }),
        menu: (baseStyles) => ({
          ...baseStyles,
          backgroundColor: 'white',
        }),
      }}
      defaultValue={selectedOption}
      onChange={setSelectedOption}
      options={options}
    />
  )
}
