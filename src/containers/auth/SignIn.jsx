import { 
  Box, 
  HStack, 
  Link, 
  Stack, 
  Text, 
  VStack,
  Alert,
  AlertIcon,
  AlertTitle } from '@chakra-ui/react'
import React, { useState } from 'react'
import { Formik } from 'formik'
import Image from 'next/image'
import * as Yup from 'yup'
import BaseButton from '../../components/Base/BaseButton'
import LoginField from '../../components/Fields/LoginField'
import { redirectLoginSSO } from '../../helpers/utils'
import { useDispatch, useSelector } from 'react-redux'
import { loginUserExt } from '../../store/actions/auth'
import { useRouter } from 'next/router'
import FacebookLogin from 'react-facebook-login'
import { useToast } from '@chakra-ui/react'


export default function SignIn() {

  // variabel 
  const dispatch = useDispatch()
  const router = useRouter()
  const toast = useToast()
  const [loading, setLoading] = useState(false)
  const [isError, setError] = useState(null)

  const SigninSchema = Yup.object().shape({
    username: Yup.string()
      .required('Username required')
      .min(3, 'Username is too short'),
    password: Yup.string()
      .required('Password required')
      .min(3, 'Password is too short'),
  })

  const handleSubmit = async (data) => {
      setLoading(true)
      if (data.username === 'admin' && data.password === 'admin') {
        toast({
          title: `Berhasil login`,
          position: 'top-right',
          status: 'success',
          duration: 2500,
          isClosable: true,
        })
        router.push(`/dashboard`)
        setLoading(false)
      } else {
        toast({
          title: `Gagal login`,
          position: 'top-right',
          status: 'error',
          duration: 2500,
          isClosable: true,
        })
        setLoading(false)
      }
  }

  const responseFacebook = (response) => {
    // console.log('response >>>', response);
    if (response.status === 'unknown' || response.status === undefined || response.error)
        return thirdPartyLoginHandler({ error: true, provider: 'facebook', response: {} })
    thirdPartyLoginHandler({ error: false, provider: 'facebook', response })
    }
  return (
    <HStack>
      <Box w="50%" bgColor="white" h="100vh" p="2">
        <Box h="97vh" borderRadius="lg" bgColor="#0a88d1">
          <Box
            display="flex"
            flexDir="column"
            justifyContent="center"
            alignItems="center"
            h="100%"
          >
            <Image
              src="/images/auth/login-ill.svg"
              width={550}
              height={550}
              alt="login-ill"
            />
          </Box>
        </Box>
      </Box>
      <Box w="50%" h="100vh">
        <Box
          w="100%"
          h="100vh"
          justifyContent="center"
          alignItems="center"
          display="flex"
          flexDirection="column"
        >
          
          <Text fontSize="xl" fontWeight="bold">
            Sign In
          </Text>
          
          {isError ? (
            <Stack
            backgroundColor="blue.50"
            borderRadius="md"
            my="6"
            display="flex"
            direction="row"
            w="55%"
          >  
            <Alert status='error'>
            <AlertIcon />
             <AlertTitle>{isError}</AlertTitle>
          </Alert>
          </Stack>
          ) : (
          <Stack
            backgroundColor="blue.50"
            p="4"
            borderRadius="md"
            my="6"
            display="flex"
            direction="row"
            w="55%"
          >
            
            <Text color="purple.600" fontSize="xs">
              Username <span style={{ fontWeight: 'bold' }}>admin</span>{' '}
              and Password <span style={{ fontWeight: 'bold' }}> admin </span> to
              continue.
            </Text>
          </Stack>)}
          <Box w="55%">
            <Formik
              initialValues={{ username: '', password: '' }}
              validationSchema={SigninSchema}
              onSubmit={(values, actions) => {
                handleSubmit(values)
                actions.resetForm()
              }}
            >
              {(formik) => (
                <VStack as="form" gap="3" onSubmit={formik.handleSubmit}>
                  <LoginField
                    label="Username"
                    name="username"
                    type="text"
                    placeholder="enter username"
                  />
                  <LoginField
                    label="Password"
                    name="password"
                    type="password"
                    placeholder="enter password"
                  />
                  <VStack w="100%" py="3">
                    <BaseButton
                      width="100%"
                      type="submit"
                      py="5"
                      bgColor="blue.400"
                      borderRadius="full"
                      isLoading={loading}
                    >
                      {loading ? 'Loading..' : 'Login'}
                    </BaseButton>
                  </VStack>
                </VStack>
              )}
            </Formik>
            <Text fontSize="sm" color="gray.400" textAlign="center">
              OR
            </Text>
            <BaseButton
              width="100%"
              type="submit"
              py="5"
              borderRadius="full"
              bgColor="transparent"
              color="blue.500"
              variant="outline"
              mb={3}
            >
               FB
            </BaseButton>
            
            <BaseButton
              width="100%"
              type="submit"
              py="5"
              borderRadius="full"
              bgColor="transparent"
              color="green.500"
              variant="outline"
            >
               Google
            </BaseButton>
          </Box>
        </Box>
      </Box>
    </HStack>
  )
}
