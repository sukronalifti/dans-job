import {
    Heading,
    Stack,
    Text,
    Card,
    Divider,
    CardHeader,
    CardBody,
    CardFooter,
    Image,
    HStack,
    Button,
  } from '@chakra-ui/react'
  import React from 'react'

  
  export default function CardCompany({ items, title, path, url }) {
    return (
      <>
        <Stack>
        <Card maxW='sm'>
            <CardHeader>
                <HStack spacing={4}>
                <Text as='b'>{title}</Text>
                <Button colorScheme='blue' variant='outline' > Other Job </Button>
                </HStack>
                <Divider />
            </CardHeader>
            <CardBody>
            <Divider />
                <Image
                src='https://images.unsplash.com/photo-1555041469-a586c61ea9bc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1770&q=80'
                alt='Green double couch with wooden legs'
                borderRadius='lg'
                />
                <Stack mt='6' spacing='3'>
                <Heading size='md'>Living room Sofa</Heading>
                <Text>
                   {url}
                </Text>
                <Text color='blue.600' fontSize='2xl'>
                    $450
                </Text>
                </Stack>
            </CardBody>
            <Divider />
            </Card>
        </Stack>
      </>
    )
  }
   