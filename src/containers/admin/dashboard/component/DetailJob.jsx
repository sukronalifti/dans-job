import { 
    Stack, 
    Button,
    Heading, 
    Text,
    Divider,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    HStack } from "@chakra-ui/react";
import React, {useEffect} from "react";
import { useRouter } from "next/router";
import { ArrowBackIcon } from '@chakra-ui/icons'
import CardWrapper from "../../../../components/Layout/CardWrapper";
import { detailTableData } from "../../../../store/actions/dashboard/table";
import { useDispatch, useSelector } from 'react-redux'
import CardCompany from "./CardCompany";

export default function DetailJob() {
    const router = useRouter()
    const dispatch = useDispatch()
    const dataStore = useSelector((state) => state.dataTable?.data)

    useEffect(() => {
        dispatch(detailTableData(router.query.id))
    }, [])

    console.table('dataStore...', dataStore)
    return (
        <>
          <Stack spacing='15px'>
            <HStack>
            <Button 
                colorScheme='blue' variant='link'
                leftIcon={<ArrowBackIcon />} 
                onClick={() =>
                router.push(`/dashboard`)
               }>
              Back
            </Button>
            </HStack>
            <CardWrapper>
                <Stack>
                    <Heading size='md' as='b' fontSize='12px'> <Text color='gray'>{dataStore?.type} / {dataStore?.location}</Text> </Heading>
                    <Heading size='md' as='b' fontSize='24px'>{dataStore?.title} </Heading>
                    <Divider />
                </Stack>
                <Stack mt={2}>
                    <Heading size='md' as='b' fontSize='18px'>{dataStore?.company} </Heading>
                    <HStack>
                        <Text w='50%'>
                            {`${dataStore?.description}`}
                        </Text>
                        <CardCompany title={dataStore?.company} url={dataStore?.url}/>
                    </HStack>
                </Stack>
                <Stack mt={4}>
                <Heading size='md' as='b' fontSize='18px'>What you'll be doing </Heading>
                <Text w='50%'>
                    {`${dataStore?.how_to_apply}`}
                </Text>
                </Stack>
            </CardWrapper>
          </Stack>
        </>
    )
}