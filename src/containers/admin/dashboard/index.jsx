import { 
  Flex, 
  Stack, 
  InputGroup, 
  HStack, 
  Button, 
  Input, 
  InputLeftElement,
  Checkbox,
  Text, 
  CardHeader,
  Card,
  CardBody,
  StackDivider,
  Divider,
  Box,
  SimpleGrid,
  Heading } from '@chakra-ui/react'
import { useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CardWrapper from '../../../components/Layout/CardWrapper'
import { UnlockIcon, RepeatClockIcon } from '@chakra-ui/icons'
import { useRouter } from 'next/router'
import { getTableData, filterTableData } from '../../../store/actions/dashboard/table'

export default function Dashboard() {
  const dispatch = useDispatch()
  const router = useRouter()
  const [currentPage, setCurrentPage] = useState(0)
  const [data, setData] = useState({
    description: null,
    location: null,
    full_time: false
  })
  const dataChartStore = useSelector((state) => state.dataTable)
 
  const handleFilter = async () => {
    // console.log(data)
    await dispatch(filterTableData(data))
  }

  const handleReset = async () => {
    await dispatch(getTableData(data))
    setData({
      description: null,
      location: null,
      full_time: false
    })
  }

  const handleOnChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value
    })

  }

  useEffect(() => {
    dispatch(getTableData())
  }, [])


  return (
    <>
    <Stack spacing="15px">
      <CardWrapper>
        <Flex w="100%" gap={5} flexDir={{ base: 'column', md: 'row' }}>
        <HStack spacing='24px'>
        <InputGroup >
          <InputLeftElement
            pointerEvents='none'
            children={<UnlockIcon color='gray.300' />}
          />
          <Input type='tel' name='description' htmlSize={100} placeholder='Filter by title, benefit, companies' size='md' onChange={(e) => handleOnChange(e)} defaultValue={data.description}/>
        </InputGroup>
        <InputGroup>
          <InputLeftElement
            pointerEvents='none'
            children={<RepeatClockIcon color='gray.300' />}
          />
          <Input type='tel' name='location' placeholder='Filter by city, state, zip code or country' size='md' onChange={(e) => handleOnChange(e)} defaultValue={data.location}/>
        </InputGroup>
        <InputGroup>
          <Checkbox name='full_time' colorScheme='blue' onChange={(e) => handleOnChange(e)} value={true} defaultChecked={data.full_time}>
            <Text as='b'>Full Time Only</Text>
          </Checkbox>
        </InputGroup>
        <HStack>
          <Button onClick={() => handleFilter()}>Search</Button>
          <Button onClick={() => handleReset()} colorScheme='red'>Reset</Button>
        </HStack>
        </HStack>
        </Flex>
      </CardWrapper>
      <CardWrapper>
        <Card>
          <CardHeader>
            <Heading size='md' as='b' fontSize='30px'>Job List</Heading>
            <Divider />
          </CardHeader>
          <CardBody>
           <Stack divider={<StackDivider />} spacing='4' >
           {dataChartStore?.items?.data?.map((job, index) => (
           <HStack key={index}>
            <SimpleGrid  columns={2} spacing={60} >
              <Box 
              w='100%' 
              mr={900} 
              onClick={() =>
              router.push(`/dashboard/${job.id}/detail`)
               }>
                <Heading colorScheme='blue' size='xs' textTransform='uppercase'>
                  <Text color='blue.600'>{job?.title}</Text>
                </Heading>
                <HStack >
                  <Text pt='2' color='grey' fontSize='sm'>
                    {job?.company} - 
                  </Text>
                  <Text pt='2' color='green' as='b' fontSize='sm'>
                    {job?.type}
                  </Text>
                </HStack>
              </Box>
              <Box w='70%'>
                  <Text color='grey' as='b' fontSize='12px'>{job?.location}</Text>
                <HStack>
                  <Text pt='2' color='grey' fontSize='sm'>
                    1 Day Ago 
                  </Text>
                </HStack>
              </Box>
            </SimpleGrid>
           </HStack>
           ))}
          </Stack>
        </CardBody>
        </Card>
      </CardWrapper>
    </Stack>
    </>
  )
}
