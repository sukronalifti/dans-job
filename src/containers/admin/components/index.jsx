import { Stack, Text, VStack } from '@chakra-ui/react'
import { Formik } from 'formik'
import Breadcrumbs from '../../../components/Base/Breadcrumbs'
import TextField from '../../../components/Fields/TextField'
import CheckboxField from '../../../components/Fields/CheckboxField'
import BaseButton from '../../../components/Base/BaseButton'
import BaseModal from '../../../components/Base/BaseModal'
import BaseToast from '../../../components/Base/BaseToast'
import SelectField from '../../../components/Fields/SelectField'
import CardWrapper from '../../../components/Layout/CardWrapper'
import TextArea from '../../../components/Fields/TextArea'
import * as Yup from 'yup'
import Datatable from './table/Datatable'

export default function Dashboard() {
  const selectOption = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
  ]

  const dataBreadcrumbs = [
    {
      title: 'Built-in Components',
      path: 'Built-in Components',
      href: '/components',
    },
  ]

  return (
    <>
      <Stack spacing="30px">
        {/* BREADCRUMB START */}
        <Breadcrumbs items={dataBreadcrumbs} title="Dashboard" />
        {/* BREADCRUMB END */}

        <CardWrapper>
          <Stack gap="20px">
            {/* FORM INPUT START */}
            <Text fontWeight="bold">FORM INPUT</Text>

            <Formik
              initialValues={{ username: '', password: '' }}
              validationSchema={Yup.object({
                username: Yup.string()
                  .required('Username required')
                  .min(6, 'Username is too short'),
                password: Yup.string()
                  .required('Password required')
                  .min(6, 'Password is too short'),
              })}
              onSubmit={(values, actions) => {
                alert(JSON.stringify(values, null, 2))
                actions.resetForm()
              }}
            >
              {(formik) => (
                <VStack as="form" onSubmit={formik.handleSubmit}>
                  <TextField
                    label="Username"
                    name="username"
                    type="text"
                    placeholder="enter username"
                  />
                  <TextField
                    label="Password"
                    name="password"
                    type="password"
                    placeholder="enter password"
                  />
                  <TextArea
                    label="Address"
                    name="address"
                    type="address"
                    placeholder="Address"
                  />

                  <BaseButton type="submit">Submit</BaseButton>
                </VStack>
              )}
            </Formik>

            {/* FORM INPUT END */}

            <Text fontWeight="bold">CHECKBOX INPUT</Text>

            {/* CHECKBOX START */}
            <CheckboxField text="Checkbox" />
            {/* CHECKBOX END */}

            {/* SELECT FIELD START */}
            <Text fontWeight="bold">SELECT FIELD</Text>

            <SelectField options={selectOption} />
            {/* SELECT FIELD END */}

            {/* MODAL START */}
            <Text fontWeight="bold">MODAL</Text>

            <BaseModal
              title="Delete"
              body="Are you sure? You can't undo this action afterwards."
            />
            {/* MODAL END */}

            {/* TOAST START */}
            <Text fontWeight="bold">TOAST</Text>

            <BaseToast title="Berhasil" status="success" color="green" />
            {/* TOAST END */}

            <Text fontWeight="bold">TABLE</Text>

            {/* DATATABLE & PAGINATION START */}
            <Datatable />
            {/* DATATABLE & PAGINATION END */}
          </Stack>
        </CardWrapper>
      </Stack>
    </>
  )
}
