import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { DefaultTable } from '../../../../components/Base/Table'
import { COLUMNS } from '../../../../constants/Column'
import { getTableData } from '../../../../store/actions/dashboard/table'
import DatatableFilter from './DatatableFilter'

export default function Datatable() {
  const dispatch = useDispatch()
  const [currentPage, setCurrentPage] = useState(1)
  const [startDate, setStartDate] = useState(null)
  const [endDate, setEndDate] = useState(null)

  const dataTableStore = useSelector((state) => state.dataTable)

  const start = moment(startDate).format('YYYY-MM-DD')
  const end = moment(endDate).format('YYYY-MM-DD')

  const handleReset = () => {
    setStartDate(null)
    setEndDate(null)

    if (startDate && endDate) {
      dispatch(getTableData({ from: null, to: null }))
    }
  }

  const handleFilter = () => {
    console.log('sugfsgf')

    if (startDate && endDate) {
      dispatch(getTableData({ from: start, to: end }))
    }
  }

  const handleFilterDate = (dates) => {
    const [startA, endA] = dates
    setStartDate(startA)
    setEndDate(endA)
  }

  // USING DUMMY API FOR DATATABLE
  useEffect(() => {
    dispatch(getTableData({ page: currentPage }))
  }, [currentPage])

  return (
    <>
      <DatatableFilter
        startDate={startDate}
        setStartDate={setStartDate}
        endDate={endDate}
        setEndDate={setEndDate}
        handleFilter={handleFilter}
        onChangeDate={handleFilterDate}
        handleReset={handleReset}
      />
      <DefaultTable
        data={dataTableStore?.items}
        setPage={setCurrentPage}
        columns={COLUMNS}
        pageIndex={currentPage}
        pageSize={dataTableStore?.queryPageSize}
        // totalCount={dataTableStore?.totalCount}
        pageCount={dataTableStore?.totalPages}
      />
    </>
  )
}
