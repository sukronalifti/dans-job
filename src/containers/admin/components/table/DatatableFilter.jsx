import {
  Card,
  Flex,
  FormControl,
  HStack,
  Input,
  InputGroup,
  InputLeftElement,
  Stack,
} from '@chakra-ui/react'
import { useState } from 'react'
import BaseButton from '../../../../components/Base/BaseButton'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import { HiCalendar } from 'react-icons/hi'

const dateInitialValues = {
  startDate: null,
  endDate: null,
}

export default function DatatableFilter({
  startDate,
  endDate,
  handleFilter,
  handleReset,
  onChangeDate,
}) {
  return (
    <Card variant="outline" p={3}>
      <Flex alignItems="center" gap="10">
        <Stack spacing={4} alignItems="center" className="inputDateFilter">
          <InputGroup display="flex" alignItems="center" gap={2}>
            <HiCalendar />
            <DatePicker
              selected={startDate}
              startDate={startDate}
              endDate={endDate}
              selectsRange
              onChange={(value) => onChangeDate(value)}
              placeholderText="Select Date"
            />
          </InputGroup>
        </Stack>
        <HStack spacing={3}>
          <BaseButton onClick={handleFilter}>Filter</BaseButton>
          <BaseButton variant="outline" onClick={handleReset}>
            Reset
          </BaseButton>
        </HStack>
      </Flex>
    </Card>
  )
}
