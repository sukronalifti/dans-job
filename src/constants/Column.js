import { HStack } from '@chakra-ui/react'
import { FiEdit, FiEye, FiTrash } from 'react-icons/fi'
import BaseIconButton from '../components/Base/BaseIconButton'

export const COLUMNS = [
  {
    Header: 'Author',
    accessor: 'author',
    sticky: 'left',
  },
  {
    Header: 'Title',
    accessor: 'title',
  },
  {
    Header: 'Published At',
    accessor: 'publishedAt',
  },
  {
    Header: 'Aksi',
    Footer: 'Aksi',
    Cell: ({ cell }) => (
      <>
        <HStack>
          <BaseIconButton
            value={cell.row.name}
            colorScheme="teal"
            icon={<FiEye />}
          />
          <BaseIconButton
            value={cell.row.name}
            colorScheme="green"
            icon={<FiEdit />}
          />
          <BaseIconButton
            value={cell.row.name}
            colorScheme="red"
            icon={<FiTrash />}
          />
        </HStack>
      </>
    ),
  },
]

export const COLUMNSGALERI = [
  {
    Header: 'Author',
    accessor: 'author',
    disableFilters: true,
    sticky: 'left',
  },
  {
    Header: 'Title',
    accessor: 'title',
    sticky: 'left',
  },
  {
    Header: 'Published At',
    accessor: 'publishedAt',
  },
  {
    Header: 'Aksi',
    Footer: 'Aksi',
    accessor: '',
    Cell: ({ cell }) => (
      <>
        <HStack>
          <BaseIconButton
            value={cell.row.name}
            colorScheme="teal"
            icon={<FiEye />}
          />
          <BaseIconButton
            value={cell.row.name}
            colorScheme="green"
            icon={<FiEdit />}
          />
          <BaseIconButton
            value={cell.row.name}
            colorScheme="red"
            icon={<FiTrash />}
          />
        </HStack>
      </>
    ),
  },
]
