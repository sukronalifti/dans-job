import React from 'react'
import {
  HiDocumentDuplicate,
} from 'react-icons/hi'

export const Menus = [
  {
    href: '#',
    label: 'Submenu',
    icon: <HiDocumentDuplicate size={14} />,
    subMenu: [
      {
        href: '/components',
        label: 'Built-in Components',
      },
      {
        href: '/setting/data-test',
        label: 'Data Test',
      },
    ],
  },
]
