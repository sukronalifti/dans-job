import Cookies from 'js-cookie'

export const isLogged = () => {
  const getCookie = Cookies.get('accessToken')
  return getCookie
}

export const getUserData = () => {
  const getCookie = Cookies.get('userData')
  const loginData = getCookie ? Cookies.get('userData') : null
  return loginData ? JSON.parse(loginData) : null
}


