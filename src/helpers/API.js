import axios from 'axios'
import { API_LIST_DANS, API_DETAIL_DANS } from '../constants/PatchApi'

export const APIAUTH = () => {
  
  const API = axios.create({
    baseURL: process.env.NEXT_BASE_URL,
    headers: {
      'content-Type': 'application/json',
    },
  })

  const getApiListJob = () => {
    return API.get(`${API_LIST_DANS}`)
  }

  const filterApiListJob = (payload) => {
    return API.get(`${API_LIST_DANS}?description=${payload.description}&location=${payload.location}&full_time=${payload.full_time}`)
  }

  const detailListJob = (id) => {
    return API.get(`${API_DETAIL_DANS}/${id}`)
  }

 
  return {
    getApiListJob,
    filterApiListJob,
    detailListJob,
  }
}


