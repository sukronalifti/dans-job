const withPlugins = require('next-compose-plugins')
const optimizedImages = require('next-optimized-images')
const webpack = require('webpack')
const DotEnv = require('dotenv')

DotEnv.config()

module.exports = withPlugins(
  [
    [
      optimizedImages,
      {
        images: {
          domains: ['#'],
        },
        inlineImageLimit: 8192,
        imagesFolder: 'images',
        imagesName: '[name]-[hash].[ext]',
        handleImages: ['jpeg', 'jpg', 'png', 'svg', 'webp', 'gif', 'ico'],
        optimizeImages: false,
        optimizeImagesInDev: false,
        mozjpeg: {
          quality: 80,
        },
        optipng: {
          optimizationLevel: 3,
        },
        pngquant: false,
        gifsicle: {
          interlaced: true,
          optimizationLevel: 3,
        },
        webp: {
          preset: 'default',
          quality: 75,
        },
      },
    ],
  ],
  {
    webpack: (config) => {
      config.plugins.push(new webpack.EnvironmentPlugin(process.env))
      return config
    },
  },
)
