import { extendTheme } from '@chakra-ui/react'

const theme = extendTheme({
  styles: {
    global: {
      'html,body': {
        color: 'gray.700',
      },
    },
  },
  fonts: {
    body: `'Poppins', sans-serif`,
  },
  colors: {
    brand: {
      700: '#003D35',
    },
  },
})

export default theme
